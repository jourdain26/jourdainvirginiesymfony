$(document).ready(function() {
    $('form').submit(function(e){
        e.preventDefault();

        form = $(this);
        $.ajax({
            type: "POST",
            url: '/addInscriptionAjax',
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                $(this).reset();
                alert(data); // show response from the php script.
            }
        });

    });
});