<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscriptionAction(Request $request)
    {
        // just setup a fresh $task object (remove the dummy data)
        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($user);
             $entityManager->flush();

            return $this->redirectToRoute('/');
        }

        return $this->render('user/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/inscriptionAjax", name="inscriptionajax")
     */
    public function inscriptionAjaxAction(Request $request)
    {
        // just setup a fresh $task object (remove the dummy data)
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        return $this->render('user/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/addInscriptionAjax", name="addInscriptionAjax")
     */
    public function addInscriptionAjaxAction(Request $request)
    {
        if($request->isXmlHttpRequest()) // pour vérifier la présence d'une requete Ajax
        {
            $user = new User();

            $userData = $request->request->get('user');

            $civilite = $userData['civilite'];
            $user->setCivilite($civilite);

            $firstname = $userData['firstname'];
            $user->setFirstname($firstname);

            $lastname = $userData['lastname'];
            $user->setLastname($lastname);

            $telephone = $userData['telephone'];
            $user->setTelephone($telephone);

            $email = $userData['email'];
            $user->setEmail($email);

            if($userData['newsletter'] != null){
                $user->setNewsletter($newsletter);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return new JsonResponse([
                $request
            ]);
        }else{
            return new JsonResponse([
                'success' => "false",
                'message' => "Utilisateur non renseigné"
            ]);
        }
    }
}
